package de.baierfamily.alexa.meerschweinchen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by Eltern on 24.09.2017.
 */
public class ResponsibleTable {
  private static final Logger log = LoggerFactory.getLogger(ResponsibleTable.class);
  public static final int HOUR_FROM_MORNING = 1;
  public static final int HOUR_TO_MORNING = 11;
  public static final int HOUR_FROM_DAY = 11;
  public static final int HOUR_TO_DAY = 17;
  public static final int HOUR_FROM_EVENING = 17;
  public static final int HOUR_TO_EVENING = 23;


  private List<ResponsibleDefinition> responsibles = new ArrayList<>();

  public ResponsibleTable() {
    responsibles.add(new ResponsibleDefinition(DayOfWeek.MONDAY, HOUR_FROM_MORNING, HOUR_TO_MORNING, Child.JULE));
    responsibles.add(new ResponsibleDefinition(DayOfWeek.MONDAY, HOUR_FROM_DAY, HOUR_TO_DAY, Child.ANNA));
    responsibles.add(new ResponsibleDefinition(DayOfWeek.MONDAY, HOUR_FROM_EVENING, HOUR_TO_EVENING, Child.SAMUEL));

    //
    responsibles.add(new ResponsibleDefinition(DayOfWeek.TUESDAY, HOUR_FROM_MORNING, HOUR_TO_MORNING, Child.JULE));
    responsibles.add(new ResponsibleDefinition(DayOfWeek.TUESDAY, HOUR_FROM_DAY, HOUR_TO_DAY, Child.ANNA));
    responsibles.add(new ResponsibleDefinition(DayOfWeek.TUESDAY, HOUR_FROM_EVENING, HOUR_TO_EVENING, Child.SAMUEL));
    //
    responsibles.add(new ResponsibleDefinition(DayOfWeek.WEDNESDAY, HOUR_FROM_MORNING, HOUR_TO_MORNING, Child.JULE));
    responsibles.add(new ResponsibleDefinition(DayOfWeek.WEDNESDAY, HOUR_FROM_DAY, HOUR_TO_DAY, Child.SAMUEL));
    responsibles.add(new ResponsibleDefinition(DayOfWeek.WEDNESDAY, HOUR_FROM_EVENING, HOUR_TO_EVENING, Child.ANNA));
    //
    responsibles.add(new ResponsibleDefinition(DayOfWeek.THURSDAY, HOUR_FROM_MORNING, HOUR_TO_MORNING, Child.JULE));
    responsibles.add(new ResponsibleDefinition(DayOfWeek.THURSDAY, HOUR_FROM_DAY, HOUR_TO_DAY, Child.SAMUEL));
    responsibles.add(new ResponsibleDefinition(DayOfWeek.THURSDAY, HOUR_FROM_EVENING, HOUR_TO_EVENING, Child.ANNA));
    //
    responsibles.add(new ResponsibleDefinition(DayOfWeek.FRIDAY, HOUR_FROM_MORNING, HOUR_TO_MORNING, Child.JULE));
    responsibles.add(new ResponsibleDefinition(DayOfWeek.FRIDAY, HOUR_FROM_DAY, HOUR_TO_DAY, Child.ANNA));
    responsibles.add(new ResponsibleDefinition(DayOfWeek.FRIDAY, HOUR_FROM_EVENING, HOUR_TO_EVENING, Child.SAMUEL));
    //
    responsibles.add(new ResponsibleDefinition(DayOfWeek.SATURDAY, HOUR_FROM_MORNING, HOUR_TO_MORNING, Child.JULE));
    responsibles.add(new ResponsibleDefinition(DayOfWeek.SATURDAY, HOUR_FROM_DAY, HOUR_TO_DAY, Child.ANNA));
    responsibles.add(new ResponsibleDefinition(DayOfWeek.SATURDAY, HOUR_FROM_EVENING, HOUR_TO_EVENING, Child.SAMUEL));
    //
    responsibles.add(new ResponsibleDefinition(DayOfWeek.SUNDAY, HOUR_FROM_MORNING, HOUR_TO_MORNING, Child.JULE));
    responsibles.add(new ResponsibleDefinition(DayOfWeek.SUNDAY, HOUR_FROM_DAY, HOUR_TO_DAY, Child.ANNA));
    responsibles.add(new ResponsibleDefinition(DayOfWeek.SUNDAY, HOUR_FROM_EVENING, HOUR_TO_EVENING, Child.SAMUEL));

  }

  public Child findResponsiblePerson(LocalDateTime localDateTime) {
    // DayOfWeek localDateTime.getDayOfWeek();
    List<ResponsibleDefinition> responsible = responsibles.stream()
            .filter(dayOfWeek (localDateTime))
            .filter(inBetween((localDateTime))).collect(Collectors.toList());
    if (responsible.size() != 1) {
      log.error("expected exactly one responsible person. Got: [" + responsible.size() + "] : " + responsible.stream().map(r -> r.getChild().name()).collect(Collectors.joining(",")));
      if(responsible.size()==0) {
        return Child.UNBEKANNT;
      }
    }
    // in any case take the first child
    return responsible.get(0).getChild();
  }

  private static Predicate<ResponsibleDefinition> dayOfWeek(LocalDateTime localDateTime) {
    Predicate<ResponsibleDefinition> filter = res -> res.getDayOfWeek().equals(localDateTime.getDayOfWeek());
    return filter;
  }

  private static Predicate<ResponsibleDefinition> inBetween(LocalDateTime localDateTime) {
    Predicate<ResponsibleDefinition> filter = res -> {
      boolean isAfterFrom = res.getHourFrom() <= localDateTime.getHour();
      boolean isBeforeTo = res.getHourTo() >= localDateTime.getHour();
      return isAfterFrom && isBeforeTo;
    };
    return filter;
  }
}

package de.baierfamily.alexa.meerschweinchen;

import java.time.DayOfWeek;

/**
 * Created by Eltern on 24.09.2017.
 */
public class ResponsibleDefinition {

  private int hourFrom;
  private int hourTo;
  private Child child;
  private DayOfWeek dayOfWeek;

  public DayOfWeek getDayOfWeek() {
    return dayOfWeek;
  }

  public void setDayOfWeek(DayOfWeek dayOfWeek) {
    this.dayOfWeek = dayOfWeek;
  }

  public int getHourFrom() {
    return hourFrom;
  }

  public void setHourFrom(int hourFrom) {
    this.hourFrom = hourFrom;
  }

  public int getHourTo() {
    return hourTo;
  }

  public void setHourTo(int hourTo) {
    this.hourTo = hourTo;
  }

  public Child getChild() {
    return child;
  }

  public void setChild(Child child) {
    this.child = child;
  }

  public ResponsibleDefinition(DayOfWeek dayOfWeek, int hourFrom, int hourTo, Child child) {

    this.dayOfWeek = dayOfWeek;
    this.hourFrom = hourFrom;
    this.hourTo = hourTo;
    this.child = child;
  }


  }

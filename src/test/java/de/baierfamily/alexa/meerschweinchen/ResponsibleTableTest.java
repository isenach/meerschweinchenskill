package de.baierfamily.alexa.meerschweinchen;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.Assert.*;

/**
 * Created by Eltern on 24.09.2017.
 */
public class ResponsibleTableTest {
  @Test
  public void findResponsiblePerson() throws Exception {
    ResponsibleTable responsibleTable = new ResponsibleTable();
    LocalDateTime sunday = LocalDateTime.of(2017, Month.SEPTEMBER, 24, 10, 58, 0);
    Child child = responsibleTable.findResponsiblePerson(sunday);
    assertEquals(Child.JULE, child);

  }@Test
  public void findResponsiblePersonForWendesdayEvening() throws Exception {
    ResponsibleTable responsibleTable = new ResponsibleTable();
    LocalDateTime sunday = LocalDateTime.of(2017, Month.SEPTEMBER, 27, 19, 58, 0);
    Child child = responsibleTable.findResponsiblePerson(sunday);
    assertEquals(Child.ANNA, child);

  }
  @Test
  public void findResponsiblePersonForNow() throws Exception {
    ResponsibleTable responsibleTable = new ResponsibleTable();
    LocalDateTime sunday = LocalDateTime.now();
    Child child = responsibleTable.findResponsiblePerson(sunday);

    assertNotNull(child);

  }
}